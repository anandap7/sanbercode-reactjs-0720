// Soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var peserta = {
    nama : "Asep",
    "jenis kelamin" : "laki-laki",
    hobi : "baca buku",
    "tahun lahir" : 1992
}

console.log(peserta)


console.log()
// Soal 2
var daftarBuah = [
    {
        nama: "strawberry",
        warna: "merah",
        "ada bijinya": "tidak",
        harga: 9000
    },
    {
        nama: "jeruk",
        warna: "oranye",
        "ada bijinya": "ada",
        harga: 8000
    },
    {
        nama: "Semangka",
        warna: "Hijau & Merah",
        "ada bijinya": "ada",
        harga: 10000
    },
    {
        nama: "Pisang",
        warna: "Kuning",
        "ada bijinya": "tidak",
        harga: 5000
    }
]

console.log(daftarBuah[0])


console.log()
// Soal 3
var dataFilm = []

function tambahFilm(nama, durasi, genre, tahun) {
    var film = {
        nama : nama,
        durasi : durasi,
        genre : genre,
        tahun : tahun
    }

    dataFilm.push(film)
}

tambahFilm("Avengers: Endgame", "3h 1m", "Adventure, Science Fiction, Action", 2019)
tambahFilm("Joker", "2h 2m", "Crime, Thriller, Drama", 2019)
dataFilm.forEach(function(item) {
    console.log(item)
})


console.log()
// Soal 4
// Release 0
class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

// Release 1
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.legs = 2
    }

    yell() {
        console.log(this.name, "teriak, Auooo")
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this.cold_blooded = true
    }

    jump() {
        console.log(this.name + " lompat, hop hop")
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() 

var kodok = new Frog("buduk")
kodok.jump()


console.log()
// Soal 5
class Clock {
    constructor({template}) {
        this.template = template
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    start() {
        this.render()
        setInterval(this.render.bind(this), 1000)
    }

    stop() {
        clearInterval()
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
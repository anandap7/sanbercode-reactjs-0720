// Soal 1
console.log('--- Jawaban Soal 1 ---')
let radius = 10

const luasLingkaran = (radius) => {
    return Math.PI * radius * radius
}

const kelilingLingkaran = (radius) => {
    return Math.PI * (2 * radius)
}

console.log(`Luas Lingkaran dengan jari2 ${radius} adalah ${luasLingkaran(radius)}`)
radius = 100
console.log(`Keliling Lingkaran dengan jari2 ${radius} adalah ${kelilingLingkaran(radius)}`)


// Soal 2
console.log('\n--- Jawaban Soal 2 ---')
let kalimat = ""

var tambahKata = (kata) => {
    kalimat += `${kata} `
    console.log(kalimat)
}

tambahKata('saya')
tambahKata('adalah')
tambahKata('seorang')
tambahKata('frontend')
tambahKata('developer')


// Soal 3
console.log('\n--- Jawaban Soal 3 ---')
class Book{
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}

class Comic extends Book{
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price)
        this.isColorful = isColorful
    }
}

const buku = new Book('Cara Bikin Komik', 100, 30000)
console.log(buku)
const komik = new Comic('Nussa & Rara', 50, 50000, true)
console.log(komik)
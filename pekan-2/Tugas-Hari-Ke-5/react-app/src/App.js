import React from 'react';
import './style.css'

function App() {
  return (
    <div className="App">
      <table>
        <tr><td colSpan="2"><h1>Form Pembelian Buah</h1></td></tr>
        <tr>
          <td><strong><label for="name">Nama Pelanggan</label></strong></td>
          <td><input type="text" name="name" id="name"/></td>
        </tr>
        <tr>
          <td><strong>Daftar Item</strong></td>
          <td>
            <input type="checkbox" name="item" value="semangka" id="semangka"/> <label for="semangka">Semangka</label><br/>
            <input type="checkbox" name="item" value="jeruk" id="jeruk"/> <label for="jeruk">Jeruk</label><br/>
            <input type="checkbox" name="item" value="nanas" id="nanas"/> <label for="nanas">Nanas</label><br/>
            <input type="checkbox" name="item" value="salak" id="salak"/> <label for="salak">Salak</label><br/>
            <input type="checkbox" name="item" value="anggur" id="anggur"/> <label for="anggur">Anggur</label><br/>
          </td>
        </tr>
        <tr>
          <input type="submit" value="Kirim" />
        </tr>
      </table>
    </div>
  );
}

export default App;

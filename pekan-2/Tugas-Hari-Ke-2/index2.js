var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var time = 10000
var counter = 0
function startRead() {
    readBooksPromise(time, books[counter])
        .then(function(sisa) {
            if(books[counter + 1] != null) {
                time = sisa
                counter++
                startRead()
            }
        })
}

startRead()
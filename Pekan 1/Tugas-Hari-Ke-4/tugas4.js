// Soal 1
var counter = 0
console.log('LOOPING PERTAMA')
while (counter < 20) {
    counter += 2
    console.log(counter + ' - I love coding')
}

console.log('LOOPING KEDUA')
while(counter > 0) {
    console.log(counter + ' - I will become a frontend developer')
    counter -= 2
}


console.log()
// Soal 2
for(counter = 1; counter <= 20; counter++) {
    var string = counter;
    if(counter % 2 == 1) {
        if(counter % 3 == 0) string += ' - I Love Coding'
        else string += ' - Santai'
    }
    else if(counter % 2 == 0) string += ' - Berkualitas'
    console.log(string)
}


console.log()
// Soal 3
for(row = 1; row <= 7; row++) {
    var print = ''
    for(col = 1; col <= row; col++) {
        print += '#  '
    }
    console.log(print)
}


console.log()
// Soal 4
var kalimat="saya sangat senang belajar javascript"
var kata = kalimat.split(' ')
console.log(kata)


console.log()
// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
for(counter = 0; counter < daftarBuah.length; counter++){
    console.log(daftarBuah.sort()[counter])
}
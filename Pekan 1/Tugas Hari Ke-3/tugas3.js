// Soal 1
var kataPertama = "saya"
var kataKedua = "senang"
var kataKetiga = "belajar"
var kataKeempat = "javascript"

var jawaban1 = kataPertama + ' ' + kataKedua.charAt(0).toUpperCase() + kataKedua.substr(1) + ' ' + kataKetiga + ' ' + kataKeempat.toUpperCase()
console.log(jawaban1, '\n')


// Soal 2
var kataPertama = "1"
var kataKedua = "2"
var kataKetiga = "4"
var kataKeempat = "5"

var jawaban2 = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) +parseInt(kataKeempat)
console.log(jawaban2, '\n')


// Soal 3
var kalimat = 'wah javascript itu keren sekali'

var kataPertama = kalimat.substring(0, 3)
var kataKedua   = kalimat.substring(4, 14) // do your own! 
var kataKetiga  = kalimat.substring(15, 18) // do your own! 
var kataKeempat = kalimat.substring(19, 24) // do your own! 
var kataKelima  = kalimat.substring(25, 31) // do your own! 

console.log('Kata Pertama: ' + kataPertama) 
console.log('Kata Kedua: ' + kataKedua) 
console.log('Kata Ketiga: ' + kataKetiga) 
console.log('Kata Keempat: ' + kataKeempat) 
console.log('Kata Kelima: ' + kataKelima)
console.log()


// Soal 4
var nilai = 89, indeks

if(nilai >= 0 && nilai <= 100){
    if(nilai >= 80) indeks = 'A'
    else if(nilai >= 70) indeks = 'B'
    else if(nilai >= 60) indeks = 'C'
    else if(nilai >= 50) indeks = 'D'
    else if(nilai < 50) indeks = 'E'
}
else indeks = '-_-'

console.log(indeks, '\n')


// Soal 5
var tanggal = 20
var bulan = 4
var tahun = 1998

switch(bulan){
    case 1:
        bulan = 'Januari'
        break
    case 2:
        bulan = 'Februari'
        break
    case 3:
        bulan = 'Maret'
        break
    case 4:
        bulan = 'April'
        break
    case 5:
        bulan = 'Mei'
        break
    case 6:
        bulan = 'Juni'
        break
    case 7:
        bulan = 'Juli'
        break
    case 8:
        bulan = 'Agustus'
        break
    case 9:
        bulan = 'September'
        break
    case 10:
        bulan = 'Oktober'
        break
    case 11:
        bulan = 'November'
        break
    case 12:
        bulan = 'Desember'
        break
}

var jawaban5 = tanggal + ' ' + bulan + ' ' + tahun
console.log(jawaban5, '\n')